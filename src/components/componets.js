
import { StyleSheet, Text, View, ScrollView, TextInput, Button } from "react-native";
import { useState, useEffect} from "react";
//import { StyleSheet, Text, View, ScrollView } from 'react';


export default function Component() {
    const [text, setText] = useState("");
    const [numLetras, setNumLetras,] = useState(0);
    function click(){
        setNumLetras(text.length)
    }
    return (


        <View style={styles.container}>
            <Text style={styles.Text}>Vilaça na régua{text}</Text>
            <TextInput
            style={styles.Input}
        placeholder="digite algo..."
        value={text}
        onChangeText={(TextInput)=> setText(TextInput)}
            />
            <Button
                title="Aperte Aqui"
                onPress={()=>{click();
                }}
            />

            {numLetras > 0 ? 
            <Text> {numLetras}</Text>
                :
            <Text></Text>
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center'
    },

    Text:{
        fontSize:24,
        fontWeight:'bold'
    },

    Input:{
        borderWidth:1,
        borderColor:'gray',
        width:'80%',
        padding:10,
        marginVertical:10
    },

});
