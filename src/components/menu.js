import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';


export default function Menu({navigation}) {
    return (

        <View style={styles.container}>
            <TouchableOpacity style={styles.menu}
                onPress={() => navigation.navigate('TelaInicial')}>
                <Text>Opção1</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu}
                onPress={() => navigation.navigate('SegundaTela')}>
                <Text>Opção2</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu}>
                <Text>Opção3</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    menu: {
        padding: 10,
        margin: 5,
        backgroundColor: "blue",
        borderRadius: 5,
    }

});
