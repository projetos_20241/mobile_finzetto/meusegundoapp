import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import Layout from './layoutDeTelaEstrutura';

export default function LayoutHorizontal() {
    return (
        <ScrollView horizontal={true}>
            <View style={styles.box1}></View>
            <View style={styles.box2}></View>
            <View style={styles.box3}></View>
            <View style={styles.box1}></View>
            <View style={styles.box2}></View>
            <View style={styles.box3}></View>
            <View style={styles.box1}></View>
            <View style={styles.box2}></View>
            <View style={styles.box3}></View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {

        flex: 1,
        flexDirection: "row",  // Horizntal

    },

    box1: {
        marginTop: 40,
        width: 50,
        height: 50,
        backgroundColor: 'red',
        //borderRadius: 100,
    },
    box2: {
        marginTop: 40,
        width: 50,
        height: 50,
        backgroundColor: 'green',
        //borderRadius: 100,
    },
    box3: {
        marginTop: 40,
        width: 50,
        height: 50,
        backgroundColor: 'blue',
        //borderRadius: 100,
    },

    

});
