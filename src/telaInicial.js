import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const TelaInicial = () => {
    return (
        <View style={styles.container}>
            <Text>Este é o componente TelaInicial</Text>
        </View>
    );
};

export default TelaInicial;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});


